const mongoose = require("mongoose");

//IMPROVEMENT: could add payment method and add different types of tesla
const ConfigurationSchema = new mongoose.Schema(
  {
    chatId: { type: String },
    type: { type: String, required: true },
    color: { type: String, required: true },
    wheels: { type: String, required: true },
    winter_wheels: { type: Boolean, required: true },
    interior: { type: String, required: true },
    advanced_autopilot: { type: Boolean, required: true },
    max_performance_autopilot: { type: Boolean, required: true },
    img: {
      data: Buffer,
      contentType: String,
    },
    price: { type: Number, required: true },
  },
  { timestamps: true }
);

module.exports = mongoose.model("TslaConfiguration", ConfigurationSchema);
