const puppeteer = require("puppeteer");
const path = "https://www.tesla.com/it_it/model3/design#overview";
//TODO: add switch for different kind of tesla
const webScraper = {
  openSession: () => {}, //EVALUATE IF GET THIS COMMAND OR IF IT'S USELESS
  setModel: (type) => {
    //example
    (async () => {
      //TODO: this could be added in a function
      const browser = await puppeteer.launch({
        ignoreHTTPSErrors: true,
        headless: false,
        defaultViewport: {
          width: 900,
          height: 500,
        },
        args: [
          "--start-maximized", // make page full screen
        ],
      });
      const page = await browser.newPage();
      // Configure the navigation timeout
      await page.setDefaultNavigationTimeout(0);
      await page.goto(path);

      //TODO: ADD switch for types

      //'input[id="$MT328-Model 3 Long Range"]'
      await page
        .$eval('input[name="BATTERY_AND_DRIVE"][value="$MT328"]', (e) =>
          e.click()
        )
        .catch((e) => console.log(e)); //TODO:WORK'IN*/

      await browser.close();

      //const m3Types=[];
      //const html = await page.$eval('input[name="BATTERY_AND_DRIVE"][value="$MT328"]', (e) => e.outerHTML); //TODO:WORK'IN
      /*const m3Types = await page.$$eval(
        'input[name="BATTERY_AND_DRIVE"]',
        (inputs) => inputs.map((input) => input.outerHTML)
      ); //TODO:WORK'IN*/

      //console.log(m3Types);

      //just need [value="$PMNG"]
      /* await page
        .$eval('input[name="PAINT"][value="$PMNG"]', (e) => e.click())
        .catch((e) => console.log(e));*/
      //})();
    })();
  },
  /*getImage: async () => {
    const image = await page.$eval('svg[id="Model_3_Front_View"]', (e) => e.outerHTML);
    const html = await page.$eval('input[preserveAspectRatio="xMidYMid meet"]', (e) => e.outerHTML);
  },
  sendPhoto:  () => {
    const image = await page.$eval('svg[id="Model_3_Front_View"]', (e) => e.outerHTML);
    const html = await page.$eval('input[preserveAspectRatio="xMidYMid meet"]', (e) => e.outerHTML);
  },*/
  closeSession: () => {}, //EVALUATE IF GET THIS COMMAND OR IF IT'S USELESS
};

module.exports = webScraper;
