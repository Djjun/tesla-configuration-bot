const express = require("express");
const app = express();
require("dotenv").config();

//require("./bot/index.js");

const telegramBot = require("./bot/commands/commands");

// listen bot telegram
telegramBot.commands();

//TODO:add db connection implementation

// index page
const port = process.env.PORT || 3000;

/*router.get('/', (req, res) => {
    // index html page
    res.sendFile(path.join(__dirname + '/index.html'));
});

app.use('/', router)*/

app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});
