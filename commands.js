const commandsConfig = {
  commands: [
    { command: "start", description: "Start Tesla Model 3 configuration" },
    { command: "new_configuration", description: "Start Tesla Model 3 configuration" },
    { command: "edit_configuration", description: "Start Tesla Model 3 configuration" },
    { command: "delete_configuration", description: "Start Tesla Model 3 configuration" },
    { command: "type", description: "Check Tesla Model 3 type" },
    { command: "color", description: "Check Tesla Model 3 Color" },
    { command: "interior", description: "Check Tesla Model 3 interior" },
    { command: "price", description: "Get Tesla price by your configuration" },
    { command: "menu", description: "Display commands menu" },
  ],
  options: { language_code: "en" },
};

module.exports = {
  commands: commandsConfig.commands,
  options: commandsConfig.options,
};
