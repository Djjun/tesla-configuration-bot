process.env["NTBA_FIX_319"] = 1;
const TelegramBot = require("node-telegram-bot-api");
const puppeteer = require("puppeteer");
const keyboard = require("./keyboards");
const { commands, options } = require("./commands");
require("dotenv").config();

//TODO: add all tesla types
const path = "https://www.tesla.com/it_it/model3/design#overview";

/*let type,
  color,
  wheel,
  winterWheel,
  interior,
  advancedAutopilot,
  fullSelfDriving;*/

/*const localStorage = await page.evaluate(() => localStorage.getItem("app-find-my-tesla.configuration"));
        console.log(localStorage);*/

//WITH THIS METHOD WE NEED TO REFRESH PAGE
//const localStorage = await page.evaluate(() => localStorage.getItem("app-find-my-tesla.configuration"));
//await page.evaluate(() => localStorage.setItem("app-find-my-tesla.configuration", '{"value":{"StoredConfiguration":{"m3":{"user_selected_options":["$PPSW","$MT328"],"option_codes":["$W41B","$MDL3","$CPF0","$APBS","$SC04","$PPSW","$MT328","$DV4W","$PRM31","$IPB1"],"effective_date":"2022-03-24T00:00:00.000Z","lexicon_name":"Marketing IT 2022.Mar.24 3099.4856.13736 [it]"}},"Pricing":{"financeType":"lease.lease"}},"type":"object","timestamp":1653756721627,"version":"v0028d202205260412","expires":1656348721627}'));

// Create a bot that uses 'polling' to fetch new updates

const bot = new TelegramBot(process.env.BOT_TOKEN, {
  polling: true,
});
bot.setMyCommands(commands, options);

//TODO: REMEMBER YOU NEED TO DO GET AND SET METHOD FOR GETTING VALUE CHOSE BY USER, GETTING VALUE AVAILABLE FROM WEBSITE, AND SETTING IT ON WEBSITE

//create an async call for all request (commands)
/*(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto('https://news.ycombinator.com', {
    waitUntil: 'networkidle2',
  });
  await page.pdf({ path: 'hn.pdf', format: 'a4' });

  await browser.close();
})();*/

puppeteer
  .launch({
    ignoreHTTPSErrors: true,
    headless: false,
    defaultViewport: {
      width: 900,
      height: 500,
    },
    args: [
      "--start-maximized", // make page full screen
    ],
  })
  .then(async (browser) => {
    const page = await browser.newPage();
    // Configure the navigation timeout
    await page.setDefaultNavigationTimeout(0);
    await page.goto(path);

    bot.onText(/\/start/, (msg) => {
      // listens for "/start" and responds with the greeting below.
      bot.sendMessage(
        msg.chat.id,
        "Hey, I'm a Tesla Configuration Telegram bot. Here you can set up your Tesla!"
      );
    });

    bot.onText(/\/click/, (msg) => {
      (async () => {
        //TODO: or first evaluate and next click literaly or

        //'input[id="$MT328-Model 3 Long Range"]'
        //input[name="BATTERY_AND_DRIVE"][value="$MT328"]
        await page
          .$eval('input[name="BATTERY_AND_DRIVE"][value="$MT328"]', (e) =>
            e.click()
          )
          .catch((e) => console.log(e)); //TODO:WORK'IN

        //const m3Types=[];
        //const html = await page.$eval('input[name="BATTERY_AND_DRIVE"][value="$MT328"]', (e) => e.outerHTML); //TODO:WORK'IN
        /*const m3Types = await page.$$eval(
          'input[name="BATTERY_AND_DRIVE"]',
          (inputs) => inputs.map((input) => input.outerHTML)
        ); //TODO:WORK'IN*/

        //console.log(m3Types);

        //just need [value="$PMNG"]
        /* await page
          .$eval('input[name="PAINT"][value="$PMNG"]', (e) => e.click())
          .catch((e) => console.log(e));*/
      })();
    });
    /*bot.onText(/\/start/, (msg) => {
    defaultKeyboard(msg.chat.id)
});*/

    // Matches "/echo [whatever]"
    bot.onText(/\/new_configuration/, (msg, match) => {
      (async () => {
        bot
          .sendMessage(
            msg.chat.id,
            "I'm inside Tesla website. Ready to configure your car"
          )
          .catch((error) => {
            console.log(error.code); // => 'ETELEGRAM'
            console.log(error.response.body); // => { ok: false, error_code: 400, description: 'Bad Request: chat not found' }
          });

        //await browser.close();
      })();
    });

    bot.onText(/\/price/, (msg) => {
      (async () => {
        // const price = await page.$eval(".finance-type finance-type--cash");
        // console.log(price);
      })();
    });

    // Matches "/color [whatever]"
    //TODO: fare in modo che esca il menù voci
    bot.onText(/\/color/, (msg) => {
      bot.sendMessage(chatId, "URL has been successfully saved!", {
        reply_markup: {
          inline_keyboard: [
            [
              {
                text: "Development",
                callback_data: "development",
              },
              {
                text: "Lifestyle",
                callback_data: "lifestyle",
              },
              {
                text: "Other",
                callback_data: "other",
              },
            ],
          ],
        },
      });
      //const resp = match[1]; //the captured "whatever"
      console.log(resp);
      // send back the matched "whatever" to the chat
      bot.sendMessage(chatId, resp);
    });

    // Listener (handler) for telegram's /label event
    bot.onText(/\/label/, (msg, match) => {
      const chatId = msg.chat.id;
      const url = match.input.split(" ")[1];

      if (url === undefined) {
        bot.sendMessage(chatId, "Please provide URL of article!");
        return;
      }

      tempSiteURL = url;
      bot.sendMessage(chatId, "URL has been successfully saved!", {
        reply_markup: {
          inline_keyboard: [
            [
              {
                text: "Development",
                callback_data: "development",
              },
              {
                text: "Lifestyle",
                callback_data: "lifestyle",
              },
              {
                text: "Other",
                callback_data: "other",
              },
            ],
          ],
        },
      });
    });
  });

// Matches "/echo [whatever]"
bot.onText(/\/echo (.+)/, (msg, match) => {
  // 'msg' is the received Message from Telegram
  // 'match' is the result of executing the regexp above on the text content
  // of the message

  const chatId = msg.chat.id;
  const resp = match[1]; // the captured "whatever"
  console.log(resp);
  // send back the matched "whatever" to the chat
  bot.sendMessage(chatId, "Command not recognized");
});

// expressApp.use(bot.webhookCallback('/secret-path'))
// bot.telegram.setWebhook('<YOUR_CAPSULE_URL>/secret-path')

// bot.launch()

/************************ EXTRA */
// Keyboard layout for requesting phone number access
/*const requestPhoneKeyboard = {
  reply_markup: {
    one_time_keyboard: true,
    keyboard: [
      [
        {
          text: "My phone number",
          request_contact: true,
          one_time_keyboard: true,
        },
      ],
      ["Cancel"],
    ],
  },
};*/

// Listener (handler) for retrieving phone number
bot.onText(/\/keyboard/, (msg) => {
  bot.sendMessage(
    msg.chat.id,
    "Can we get access to your phone number?",
    keyboard("default")
  );
});
// Listener (handler) for retrieving phone number
// bot.onText(/\/phone/, (msg) => {
//   bot.sendMessage(
//     msg.chat.id,
//     "Can we get access to your phone number?",
//     requestPhoneKeyboard
//   );
// });

// Inline keyboard options
const inlineKeyboard = {
  reply_markup: {
    inline_keyboard: [
      [
        {
          text: "YES",
          callback_data: JSON.stringify({
            command: "mycommand1",
            answer: "YES",
          }),
        },
        {
          text: "NO",
          callback_data: JSON.stringify({
            command: "mycommand1",
            answer: "NO",
          }),
        },
      ],
    ],
  },
};

// Listener (handler) for callback data from /label command
bot.on("callback_query", (callbackQuery) => {
  const message = callbackQuery.message;
  const category = callbackQuery.data;

  URLLabels.push({
    url: tempSiteURL,
    label: category,
  });

  tempSiteURL = "";

  bot.sendMessage(
    message.chat.id,
    `URL has been labeled with category "${category}"`
  );
});
